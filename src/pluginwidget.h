/*
 * Copyright 2008 George Goldberg <grundleborg@googlemail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PLASMAPLUGIN_PLUGINWIDGET_H
#define PLASMAPLUGIN_PLUGINWIDGET_H

#include <qtbrowserplugin.h>

#include <QtCore/QString>

#include <QtGui/QWidget>

class FullView;

class KComponentData;

class QHBoxLayout;

class PluginWidget : public QWidget
{
    Q_OBJECT
    
    Q_PROPERTY(QString applet READ applet WRITE setApplet)

    Q_CLASSINFO("MIME", "application/plasmoid:pls:Plasmoid")
    Q_CLASSINFO("ToSuperClass", "PluginWidget")
    Q_CLASSINFO("DefaultProperty", "applet")

public:
    PluginWidget(QWidget *parent = 0);
    QString applet();
    void setApplet(QString applet);

private:
    QHBoxLayout *m_layout;
    FullView *m_view;
    KComponentData *m_componentData;
    QString m_applet;
};

#endif //HEADER GUARD
