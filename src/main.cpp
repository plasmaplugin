/*
 * Copyright 2008 George Goldberg <grundleborg@googlemail.com>
 * Based on the plasmoid viewer tool,
 *   Copyright 2007 Frerich Raabe <raabe@kde.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "pluginwidget.h"

#include "fullview.h"

#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>

using namespace Plasma;

static const char description[] = I18N_NOOP( "Run Plasma applets in a nsplugin supporting web browser" );
static const char app_version[] = "0.1";

int main(int argc, char **argv)
{
    KAboutData aboutData( "plasmaplugin", 0, ki18n( "Plasma Web Browser Plugin" ),
                          app_version, ki18n( description ), KAboutData::License_BSD,
                         ki18n( "(C) 2007, The KDE Team" ) );
    aboutData.setProgramIconName( "plasma" );
    aboutData.addAuthor( ki18n( "George Goldberg" ),
                         ki18n( "Original author" ),
                        "grundleborg@googlemail.com" );
    KCmdLineArgs::init( argc, argv, &aboutData );
    KApplication app;

    PluginWidget pluginWidget;
    pluginWidget.setApplet("clock");
    pluginWidget.show();

    return app.exec();
}
